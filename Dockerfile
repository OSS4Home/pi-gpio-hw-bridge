FROM schachr/raspbian-stretch

# Install wiringPi
RUN apt-get update && apt-get install -y build-essential openjdk-8-jdk openjdk-8-jre git curl sudo
RUN git clone git://git.drogon.net/wiringPi
RUN cd wiringPi && ./build
RUN curl -s get.pi4j.com | sudo bash

ADD target/pi-gpio-hw-bridge-1.0.0.jar /opt/pi-gpio-hw-bridge-1.0.0.jar 

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/opt/pi-gpio-hw-bridge-1.0.0.jar"]