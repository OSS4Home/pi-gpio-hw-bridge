package de.os4home.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import de.oss4home.swagger.generate.model.Error;
import de.oss4home.swagger.generate.model.ErrorBuilder;

@ControllerAdvice
@RequestMapping(produces = "application/vnd.error+json")
public class NotFoundExceptionHandler {
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Error> notFoundException(final NotFoundException e) {
	return new ResponseEntity<>(new ErrorBuilder().code(404).message(e.getMessage()).build(), HttpStatus.NOT_FOUND);
    }

}