package de.oss4home.cc.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "")
public class PushEvent {

    private String url = null;
    private String deviceid = null;
    private Integer valueid = null;
    private String value = null;

    /**
     **/
    @ApiModelProperty(value = "")
    @JsonProperty("url")
    public String getUrl() {
	return url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    /**
     **/
    @ApiModelProperty(value = "")
    @JsonProperty("deviceid")
    public String getDeviceid() {
	return deviceid;
    }

    public void setDeviceid(String deviceid) {
	this.deviceid = deviceid;
    }

    /**
     **/
    @ApiModelProperty(value = "")
    @JsonProperty("valueid")
    public Integer getValueid() {
	return valueid;
    }

    public void setValueid(Integer valueid) {
	this.valueid = valueid;
    }

    /**
     **/
    @ApiModelProperty(value = "")
    @JsonProperty("value")
    public String getValue() {
	return value;
    }

    public void setValue(String value) {
	this.value = value;
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class PushEvent {\n");

	sb.append("  url: ").append(url).append("\n");
	sb.append("  deviceid: ").append(deviceid).append("\n");
	sb.append("  valueid: ").append(valueid).append("\n");
	sb.append("  value: ").append(value).append("\n");
	sb.append("}\n");
	return sb.toString();
    }
}
