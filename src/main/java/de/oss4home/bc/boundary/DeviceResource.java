package de.oss4home.bc.boundary;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import de.oss4home.bc.controller.DevicesController;
import de.oss4home.swagger.generate.api.DevicesApi;
import de.oss4home.swagger.generate.model.Device;
import de.oss4home.swagger.generate.model.UpdateDevice;
import de.oss4home.swagger.generate.model.Value;

@RestController
public class DeviceResource implements DevicesApi {

    @Autowired
    DevicesController controller;

    @Override
    public void changeDeviceSettings(String deviceId, UpdateDevice setting) {

    }

    @Override
    public void changeDeviceValue(String deviceId, String valueId, Value value) {
	controller.changeDeviceValue(deviceId, valueId, value);
    }

    @Override
    public Device getDevice(String deviceId) {
	return controller.getDevices(deviceId);
    }

    @Override
    public Value getDeviceValue(String deviceId, String valueId) {
	return controller.getDeviceValue(deviceId, valueId);
    }

    @Override
    public List<Value> getDeviceValues(String deviceId) {
	return controller.getDeviceValues(deviceId);
    }

    @Override
    public List<Device> getDevices(String devicetype, String sytemtype) {
	return controller.getDevices(devicetype, sytemtype);
    }

}
