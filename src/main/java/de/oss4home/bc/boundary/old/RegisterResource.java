// package de.oss4home.bc.boundary.old;
//
// import javax.ws.rs.core.Response;
//
// import org.springframework.beans.factory.annotation.Autowired;
//
// import de.oss4home.bc.boundary.ControllerPush;
// import de.oss4home.swagger.generate.api.NotFoundException;
// import de.oss4home.swagger.generate.api.RegisterApi;
// import de.oss4home.swagger.generate.model.RegisterType;
//
// public class RegisterResource implements RegisterApi {
//
// @Autowired
// ControllerPush controllerPush;
//
// @Override
// public Response registerPush(RegisterType value) throws NotFoundException {
// controllerPush.register(value.getUrl());
// return Response.ok().build();
// }
//
// }
