// package de.oss4home.bc.boundary.old;
//
// import javax.ws.rs.core.Response;
//
// import org.springframework.beans.factory.annotation.Autowired;
//
// import de.oss4home.bc.controller.DevicesController;
// import de.oss4home.swagger.generate.api.DevicesApi;
// import de.oss4home.swagger.generate.api.NotFoundException;
// import de.oss4home.swagger.generate.model.UpdateDevice;
// import de.oss4home.swagger.generate.model.Value;
//
// public class DevicesResource implements DevicesApi {
//
// @Autowired
// DevicesController controller;
//
// @Override
// public Response getDeviceValue(String deviceId, String valueId) throws
// NotFoundException {
// return Response.ok(controller.getDeviceValue(deviceId, valueId)).build();
// }
//
// @Override
// public Response getDeviceValues(String deviceId) throws NotFoundException {
// return Response.ok(controller.getDeviceValues(deviceId)).build();
// }
//
// @Override
// public Response getDevices(String devicetype, String sytemtype) throws
// NotFoundException {
// return Response.ok(controller.getDevices(devicetype, sytemtype)).build();
// }
//
// @Override
// public Response changeDeviceValue(String deviceId, String valueId, Value
// value) throws NotFoundException {
// controller.changeDeviceValue(deviceId, valueId, value);
// return Response.ok().build();
// }
//
// @Override
// public Response changeDeviceSettings(String deviceId, UpdateDevice setting)
// throws NotFoundException {
// return Response.ok().build();
// }
//
// @Override
// public Response getDevice(String deviceId) throws NotFoundException {
// return Response.ok(controller.getDevices(deviceId)).build();
// }
//
// }
