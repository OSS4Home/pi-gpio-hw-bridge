package de.oss4home.bc.boundary;

import com.pi4j.io.gpio.PinState;

public interface ControllerPush {
    void register(String url);

    void pushEvent(int id, PinState value);

}
