package de.oss4home.bc.boundary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import de.oss4home.bc.entity.Device;

@Service
public class GpioServiceImpl implements GpioService, GpioPinListenerDigital {

    @Autowired
    ControllerPush controllerPush;

    HashMap<Pin, Device> data = new HashMap<>();
    HashMap<Pin, GpioPinDigitalInput> configurationInputs = new HashMap<>();
    HashMap<Pin, GpioPinDigitalOutput> configurationOutputs = new HashMap<>();
    final GpioController gpio = GpioFactory.getInstance();

    public GpioServiceImpl() {
	loadConfiguration();

	for (Entry<Pin, Device> entry : data.entrySet()) {
	    if (entry.getValue().isInput()) {
		GpioPinDigitalInput myButton = gpio.provisionDigitalInputPin(
			RaspiPin.getPinByAddress(entry.getValue().getId()), entry.getValue().getInputType());
		myButton.setDebounce(entry.getValue().getDebounce());
		myButton.addListener(this);
		myButton.setShutdownOptions(false);
		entry.getValue().setValue(myButton.getState());
		configurationInputs.put(entry.getKey(), myButton);
	    } else {
		GpioPinDigitalOutput pin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "MyLED");
		pin.setShutdownOptions(false);
		configurationOutputs.put(entry.getKey(), pin);
		entry.getValue().setValue(pin.getState());
	    }
	}

    }

    @Override
    public List<Device> getDevices() {
	return new ArrayList<Device>(data.values());
    }

    @Override
    public Device getDevice(int id) {
	return data.get(RaspiPin.getPinByAddress(id));
    }

    @Override
    public void updateDevice(int id, PinState value) {
	GpioPinDigitalOutput pin = configurationOutputs.get(RaspiPin.getPinByAddress(id));
	Device dev = data.get(RaspiPin.getPinByAddress(id));
	if (dev != null && pin != null) {
	    pin.setState(value);
	    dev.setValue(value);
	} else {
	    // TODO: exception
	}
    }

    @Override
    public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
	data.get(event.getPin().getPin()).setValue(event.getState());
	controllerPush.pushEvent((event.getPin()).getPin().getAddress(), event.getState());

    }

    private void loadConfiguration() {
	Device input = new Device(27, PinState.HIGH);
	input.setInput(true);
	input.setInputType(PinPullResistance.PULL_DOWN);
	input.setDebounce(20);
	data.put(RaspiPin.getPinByAddress(input.getId()), input);

	Device output = new Device(1, PinState.HIGH);
	output.setInput(false);
	data.put(RaspiPin.getPinByAddress(output.getId()), output);
	output.setNegativeLogic(true);

    }

}
