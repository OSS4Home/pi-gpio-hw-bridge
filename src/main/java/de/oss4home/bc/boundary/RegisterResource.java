package de.oss4home.bc.boundary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import de.oss4home.swagger.generate.api.RegisterApi;
import de.oss4home.swagger.generate.model.RegisterType;

@RestController
public class RegisterResource implements RegisterApi {

    @Autowired
    ControllerPush controllerPush;

    @Override
    public void registerPush(RegisterType value) {
	controllerPush.register(value.getUrl());

    }

}
