package de.oss4home.bc.boundary;

import java.util.List;

import com.pi4j.io.gpio.PinState;

import de.oss4home.bc.entity.Device;

public interface GpioService {

    List<Device> getDevices();

    Device getDevice(int id);

    void updateDevice(int id, PinState value);

}
