package de.oss4home.bc.boundary;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.springframework.stereotype.Service;

import com.pi4j.io.gpio.PinState;

import de.oss4home.cc.dto.PushEvent;

@Service
public class ControllerPushImpl implements ControllerPush {

    private HashMap<String, Calendar> pushUris = new HashMap<>();

    private ClientConfig cc = null;
    private Client client = null;

    public ControllerPushImpl() {
	cc = new ClientConfig().register(new JacksonFeature());
	client = JerseyClientBuilder.newClient(cc);
    }

    @Override
    public void register(String url) {
	pushUris.put(url, Calendar.getInstance());

    }

    @Override
    public void pushEvent(int id, PinState value) {
	PushEvent event = new PushEvent();
	event.setUrl("http://192.168.22.128:5550");
	event.setDeviceid("" + id);
	event.setValueid(0);
	event.setValue(value == PinState.HIGH ? "true" : "false");
	for (Entry<String, Calendar> entry : pushUris.entrySet()) {

	    try {
		WebTarget target = client.target(entry.getKey()).path("rest").path("ccapi").path("v2")
			.path("controller").path("push");
		target.request(MediaType.APPLICATION_JSON).post(Entity.entity(event, MediaType.APPLICATION_JSON));

	    } catch (Exception ex) {

	    }

	}

    }

}
