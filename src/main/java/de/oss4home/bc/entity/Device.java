package de.oss4home.bc.entity;

import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;

public class Device {
    private int id;
    private PinState value;
    private boolean input;
    private int debounce;
    private PinPullResistance inputType;
    private int pulseTime;
    boolean negativeLogic = false;

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public PinState getValue() {
	return value;
    }

    public void setValue(PinState value) {
	this.value = value;
    }

    public Device(int id, PinState value) {
	super();
	this.id = id;
	this.value = value;
    }

    public boolean isInput() {
	return input;
    }

    public void setInput(boolean input) {
	this.input = input;
    }

    public int getDebounce() {
	return debounce;
    }

    public void setDebounce(int debounce) {
	this.debounce = debounce;
    }

    public PinPullResistance getInputType() {
	return inputType;
    }

    public void setInputType(PinPullResistance inputType) {
	this.inputType = inputType;
    }

    public int getPulseTime() {
	return pulseTime;
    }

    public void setPulseTime(int pulseTime) {
	this.pulseTime = pulseTime;
    }

    public boolean isNegativeLogic() {
	return negativeLogic;
    }

    public void setNegativeLogic(boolean negativeLogic) {
	this.negativeLogic = negativeLogic;
    }

}
