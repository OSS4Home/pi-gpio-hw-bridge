package de.oss4home.bc.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.os4home.exception.NotFoundException;
import de.oss4home.bc.boundary.GpioService;
import de.oss4home.bc.entity.Device;
import de.oss4home.bc.utils.ConverterUtil;
import de.oss4home.swagger.generate.model.Devicetype;
import de.oss4home.swagger.generate.model.Devicetype.ValueEnum;
import de.oss4home.swagger.generate.model.UpdateDevice;
import de.oss4home.swagger.generate.model.Value;
import de.oss4home.swagger.generate.model.Value.ValuetypeEnum;

@Service
public class DeviceControllerImpl implements DevicesController {

    @Autowired
    GpioService gpioService;

    @Override
    public void changeDeviceSettings(String deviceId, UpdateDevice setting) throws NotFoundException {

    }

    @Override
    public de.oss4home.swagger.generate.model.Device getDevices(String deviceId) throws NotFoundException {
	Device currentDevice = gpioService.getDevice(Integer.parseInt(deviceId));
	de.oss4home.swagger.generate.model.Device device = new de.oss4home.swagger.generate.model.Device();
	device.setId("" + currentDevice.getId());
	device.setValues(getDeviceValues(device.getId()));
	setDeviceSettings(device);
	return device;
    }

    @Override
    public Value getDeviceValue(String deviceId, String valueId) throws NotFoundException {
	if (valueId.equals("1")) {
	    return getDeviceValues(deviceId).get(0);
	} else {
	    throw new NotFoundException("");
	}
    }

    @Override
    public List<Value> getDeviceValues(String deviceId) throws NotFoundException {
	List<Value> values = new ArrayList<Value>();
	Device device = gpioService.getDevice(Integer.parseInt(deviceId));
	if (device != null) {
	    Value value = new Value();
	    value.setId(1);
	    value.setReadonly(false);
	    value.setValuetype(ValuetypeEnum.booleanValue);
	    value.setValue(ConverterUtil.getPinStateToApiState(device.getValue(), device.isNegativeLogic()));
	    values.add(value);
	    return values;
	} else {
	    throw new NotFoundException("");
	}
    }

    @Override
    public List<de.oss4home.swagger.generate.model.Device> getDevices(String devicetype, String sytemtype)
	    throws NotFoundException {
	List<Device> devices = gpioService.getDevices();
	List<de.oss4home.swagger.generate.model.Device> resultDevices = new ArrayList<>();
	for (Device currentDevice : devices) {
	    de.oss4home.swagger.generate.model.Device device = new de.oss4home.swagger.generate.model.Device();
	    device.setId("" + currentDevice.getId());
	    device.setValues(getDeviceValues(device.getId()));
	    setDeviceSettings(device);
	    resultDevices.add(device);
	}
	return resultDevices;

    }

    private void setDeviceSettings(de.oss4home.swagger.generate.model.Device device) {
	Devicetype typ = new Devicetype();
	typ.setValue(ValueEnum.switchBinary);
	device.setDevicetype(typ);
	device.getPossibledevicetypes().add(typ);
    }

    @Override
    public void changeDeviceValue(String deviceId, String valueId, Value value) throws NotFoundException {
	Device dev = gpioService.getDevice(Integer.parseInt(deviceId));
	if (dev != null && valueId.equals("1")
		&& (value.getValue().equals("true") || value.getValue().equals("false"))) {
	    gpioService.updateDevice(Integer.parseInt(deviceId),
		    ConverterUtil.getStateToPinState(value.getValue(), dev.isNegativeLogic()));

	} else {
	    throw new NotFoundException("");
	}
    }

}
