package de.oss4home.bc.utils;

import com.pi4j.io.gpio.PinState;

public class ConverterUtil {
    public static String getPinStateToApiState(PinState state, boolean isNegativLogic) {
	if ((isNegativLogic && state == PinState.HIGH) || (!isNegativLogic && state == PinState.LOW)) {
	    return "false";
	}

	return "true";
    }

    public static PinState getStateToPinState(String state, boolean isNegativLogic) {
	if ((isNegativLogic && state.equals("true")) || (!isNegativLogic && state.equals("false"))) {
	    return PinState.LOW;
	}

	return PinState.HIGH;
    }

}
